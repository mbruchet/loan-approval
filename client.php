<?php

require 'guzzle.phar';

// Create a client for http://localhost (binding to a host is optional)
$clientLoanApproval = new Guzzle\Http\Client('https://fierce-spire-43574.herokuapp.com/');
$clientAccount = new Guzzle\Http\Client('http://1.loanapproval-63.appspot.com/manageAccounts/');
$clientApproval = new Guzzle\Http\Client('http://1.loanapproval-63.appspot.com/manageApprovals/');
$clientCheckAccount = new Guzzle\Http\Client('http://1.loanapproval-63.appspot.com/checkAccount/');
$timeout = 600;

echo " --------------------------- Add accounts --------------------------- \n";
/** Create many accounts */
$responseAccount = addAccount('high','Jordan', 1000.0,'Protin',$clientAccount);
$responseAccount = addAccount('low','Mohamed', 3000.0,'Derkaoui',$clientAccount);
$responseAccount = addAccount('low','Karim', 9000.0,'Benzema',$clientAccount);

/** Verify accounts are really added*/
$responseAccount = getAccounts($clientAccount);
echo $responseAccount->getBody();

echo " --------------------------- Add approvals --------------------------- \n";
/** Create many approvals */
$responseApproval = addApproval('protinjordan', 'accepted',$clientApproval);
$responseApproval = addApproval('derkaouimohamed', 'refused',$clientApproval);


/** Verify approvals are really added */
$responseApproval = getApprovals($clientApproval);
echo $responseApproval->getBody();

echo " --------------------------- Verify risks --------------------------- \n";
/** Verify risks */
$responseRisk = getRisk('protinjordan',$clientCheckAccount);
echo $responseRisk->getBody();
echo "\n";

$responseRisk = getRisk('derkaouimohamed',$clientCheckAccount);
echo $responseRisk->getBody();
echo "\n";

$responseRisk = getRisk('benzemakarim',$clientCheckAccount);
echo $responseRisk->getBody();
echo "\n";

echo " --------------------------- Demand of leaks --------------------------- \n";
/** Demand of leaks */
$responseLoanApproval = getApprovalCredit('protinjordan', 12000,$clientLoanApproval);
echo $responseLoanApproval->getBody();
echo "\n";

$responseLoanApproval = getApprovalCredit('derkaouimohamed', 7000,$clientLoanApproval);
echo $responseLoanApproval->getBody();
echo "\n";

$responseLoanApproval = getApprovalCredit('benzemakarim', 30000,$clientLoanApproval);
echo $responseLoanApproval->getBody();
echo "\n";
echo "\n";

/** Verify demand of leaks is added on accounts */
$responseAccount = getAccounts($clientAccount);
echo $responseAccount->getBody();


/** Delete */
/*
$responseApproval = deleteApproval('protinjordan',$clientApproval);
echo $responseApproval->getBody();
$responseAccount = deleteAccount('protinjordan',$clientAccount);
echo $responseAccount->getBody();
*/

function addAccount($risk, $firstName, $account, $lastName, $clientAccount)
{
	return $clientAccount->post('addAccount?risk='.$risk.'&prenom='.$firstName.'&compte='.$account.'&nom='.$lastName)->send();
}

function addApproval($idPersonne, $approval , $clientApproval)
{
	return $clientApproval->post('addApproval?idPersonne='.$idPersonne.'&approval='.$approval)->send();
}

function getAccounts($clientAccount)
{
	return $clientAccount->get('list')->send();
}

function getApprovals($clientApproval)
{
	return $clientApproval->get('list')->send();
}

function getRisk($idPersonne, $clientCheckAccount)
{
	return $clientCheckAccount->get('riskAccount/'.$idPersonne)->send();
}

function getApprovalCredit($idPersonne, $montant, $clientLoanApproval)
{
	return $clientLoanApproval->get('approval?idPersonne='.$idPersonne.'&montant='.$montant,[
		'timeout' => 600
	])->send();
}

function deleteApproval($idAccount, $clientApproval) {
	return $clientApproval->delete('deleteApproval/'.$idAccount)->send();
}

function deleteAccount($idAccount, $clientAccount) {
	return $clientAccount->delete('deleteAccount/'.$idAccount)->send();
}
?>
