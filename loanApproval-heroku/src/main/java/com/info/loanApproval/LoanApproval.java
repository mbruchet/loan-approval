package com.info.loanApproval;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MultivaluedHashMap;


@Path("/")
public class LoanApproval implements Url {
	
	private WebTarget webTarget;
	private Client client;
	
    @Path("approval")
	@GET
	public String demandeCredit(@Context UriInfo uriInfo) throws RuntimeException {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		String montant = queryParams.getFirst("montant");
		String idPersonne = queryParams.getFirst("idPersonne").toLowerCase();
		
		initClient();
		
		Invocation.Builder invocationBuilder =  webTarget.path(CHECK_ACCOUNT_URI+"riskAccount/"+idPersonne).request(MediaType.TEXT_PLAIN);
    	Response response = invocationBuilder.get();
    	
		if (response.getStatus() != 200) {
			String erreur =	response.readEntity(String.class); 
			response.close();
			return erreur;
		}
			
		String risk = response.readEntity(String.class);
		response.close();
		
		if (risk.equals("-1")) {
			return "Account not found.";
		}
		
		float f_montant = 0;
		try {
			f_montant = Float.parseFloat(montant);
		} catch (NumberFormatException e) {
			f_montant = 0;
		}
		
		if (f_montant < 10000F && risk.trim().equals("low")) {
			try {
				creditAccount(montant, idPersonne);
			} catch (Exception e) {
				return "Erreur de credit";
			}
			closeClient();
			return "approved";
		} else {
			Invocation.Builder invocationBuilder2 = webTarget.path(APP_MANAGER_URI+"getApproval/"+idPersonne).request(MediaType.TEXT_PLAIN);
	    	Response response2 = invocationBuilder2.get();
			if (response.getStatus() != 200) {
				String erreur =	response.readEntity(String.class);
				response.close();
				return erreur;
			}
			
			String approval = response2.readEntity(String.class);
			response2.close();
			
			if (approval.equals("-1"))
				return "Approval not found";
			
			if (approval.equals("accepted")) {
				try {
					creditAccount(montant, idPersonne);
				} catch (Exception e) {
					return "Erreur de credit";
				}
				closeClient();
				return "approved";
			} else {
				closeClient();
				return "refused";
			}
			
		}
	}
	
	private void creditAccount(String montant, String idPersonne) throws Exception {
		MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
    	formData.add("idPersonne", idPersonne);
    	formData.add("montant", montant);
    	Invocation.Builder invocationBuilder = webTarget.path(ACC_MANAGER_URI+"creditAccount").queryParam("idPersonne", idPersonne).queryParam("montant", montant).request();
    	Response response = invocationBuilder.put(Entity.form(formData));
    	if (response.getStatus() != 204) {
			String erreur =	response.readEntity(String.class);
			response.close();
			throw new Exception(erreur);
		}
			
	}
	
	private void initClient() {
	    ClientConfig config = new ClientConfig();
    	client = ClientBuilder.newClient(config);
    	webTarget = client.target(BASE_URI);
	}
	
	private void closeClient() {
        client.close();
	}
	
}

