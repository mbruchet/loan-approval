package appManager;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Approval {
	
	public static final String ACCEPTED = "accepted";
	public static final String REFUSED = "refused";

	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String idPersonne;
	
	@Persistent
	private String approval;
	
	public Approval(String idPersonne, String approval) {
		this.idPersonne = idPersonne;
		this.approval = approval;
	}

	public String getLastName() {
		return idPersonne;
	}

	public void setidPersonne(String idPersonne) {
		this.idPersonne = idPersonne;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}
	
	@Override
	public String toString() {
		StringBuilder chaine = new StringBuilder();
		chaine.append(idPersonne).append(" : ")
					.append(approval);
		return chaine.toString();
	}
}
