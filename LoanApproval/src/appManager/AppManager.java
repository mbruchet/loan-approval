package appManager;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import persistenceManager.PMF;

@Path("/")
public class AppManager {
	
	private PersistenceManager persistenceManager = PMF.getInstance().getPersistenceManager(); 

	@GET
	@Path("/list")
	@Produces(MediaType.TEXT_PLAIN)
	public String listApprovals() {
		
		String query = "select from "+Approval.class.getName();
		@SuppressWarnings("unchecked")
		List<Approval> approvals = (List<Approval>) persistenceManager.newQuery(query).execute();
		
		StringBuilder page = new StringBuilder();
		if (approvals != null && approvals.size() != 0) {
			page.append("Liste des approvals ("+approvals.size()+(")\n"));
			for (Approval approval : approvals) {
				page.append(approval.toString()).append("\n");
			}
		} else {
			page.append("Pas d'approval");
		}
		return page.toString();
	}

	
	@POST
	@Path("/addApproval")
	public String addApproval(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters(); 
		Approval approval = new Approval(queryParams.getFirst("idPersonne").toLowerCase(), queryParams.getFirst("approval"));
		persistenceManager.makePersistent(approval);
		return listApprovals();
	}

	@SuppressWarnings("unchecked")
	@DELETE
	@Path("/deleteApproval/{idPersonne}")
	public String deleteAccount(@PathParam("idPersonne")String idPersonne) {
		String query = "select from "+Approval.class.getName()+" where idPersonne == '"+idPersonne.toLowerCase()+"'";
		List<Approval> approvals =  (List<Approval>) persistenceManager.newQuery(query).execute();
		if (approvals.size()>0) {
			for (Approval approval : approvals)
				persistenceManager.deletePersistent(approval);
			return "Success !";
		}
		return "Approval not found !";
	}
	
	@GET
	@Path("/getApproval/{idPersonne}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getApproval(@PathParam("idPersonne")String idPersonne) {
		
		String query = "select from "+Approval.class.getName()+" where idPersonne == '"+idPersonne.toLowerCase()+"'";
		@SuppressWarnings("unchecked")
		List<Approval> approvals = (List<Approval>) persistenceManager.newQuery(query).execute();
		if (approvals.size() > 0) {
			return approvals.get(0).getApproval();
		}
			
		return "-1";
	}
		
}
