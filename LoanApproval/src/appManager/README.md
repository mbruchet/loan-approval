# AppManager

The **appManager** is a webservice CRUD on approval, allow to CREATE, READ, UPDATE or DELETE approvals with a firstName, lastName, id, response of the approval :

- /list => method to return all the approvals
- /addApproval => method to create an approval with JSON POST
- /getApproval/{idPersonne} => method to get an approval with the person id in param 
- /deleteApproval/{idPersonne} => method to delete an approval with the person id by using GET or DELETE HTTP Request