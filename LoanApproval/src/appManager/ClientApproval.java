package appManager;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ClientErrorException;

import org.glassfish.jersey.client.ClientConfig;

public class ClientApproval {
	
		private WebTarget webTarget;
	    private Client client;
	    private static final String BASE_URI = "http://1.loanapproval-63.appspot.com/manageApprovals/";

	    public ClientApproval() {
	        ClientConfig config = new ClientConfig();
    		client = ClientBuilder.newClient(config);
    		webTarget = client.target(BASE_URI);
	    }
	    
	    public String listApprovals() {
	    	return webTarget.path("/list").request().get(String.class);
	    }

	    public String addApproval(String idPersonne, String approval) throws ClientErrorException {	    	
	    	MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
	    	formData.add("idPersonne", idPersonne);
	    	formData.add("approval", approval);
	    	return webTarget.path("/addApproval").request().post(Entity.form(formData), String.class);		
	    }
	    
	    public String removeApproval(String idPersonne) throws ClientErrorException {
	    	return webTarget.path("/deleteApproval/"+idPersonne).request(javax.ws.rs.core.MediaType.TEXT_PLAIN).delete(String.class);
	    }

	    public void close() {
	        client.close();
	    }
	    
	    public static void main(String argv[]) throws InterruptedException{
	        ClientApproval client = new ClientApproval();
	        try{
	        	client.addApproval("testtest", Approval.ACCEPTED);
	        	System.out.println(client.listApprovals());
	        }catch(Exception e){
	            System.out.println(e);
	        }
	        client.close();
	    }	
}
