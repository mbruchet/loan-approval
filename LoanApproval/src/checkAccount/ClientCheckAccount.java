package checkAccount;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.ClientErrorException;

import org.glassfish.jersey.client.ClientConfig;

public class ClientCheckAccount {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://1.loanapproval-63.appspot.com/checkAccount/";
	
    public ClientCheckAccount() {
        ClientConfig config = new ClientConfig();
        client = ClientBuilder.newClient(config);
        webTarget = client.target(BASE_URI);
    }
    
    public String getRisk(String accountId) throws ClientErrorException {	    	
        return webTarget.path("/riskAccount/"+accountId).request().get(String.class);
    }
    
   
    public void close() {
        client.close();
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) throws InterruptedException{
		ClientCheckAccount client = new ClientCheckAccount();
		 try{
	           	System.out.println(client.getRisk("protinjordan"));
	        }catch(Exception e){
	            System.out.println(e);
	        }
	        client.close();
	}

}
