package checkAccount;

import java.util.Iterator;
import java.util.List;

import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import persistenceManager.PMF;
import accManager.Account;

@Path("/")
public class CheckAccount {

	private PersistenceManager persistenceManager = PMF.getInstance().getPersistenceManager(); 

	@SuppressWarnings("unchecked")		
	@GET 
	@Path("/riskAccount/{accountId}")
	@Produces(MediaType.TEXT_PLAIN)
	public String riskAccount(@PathParam("accountId")String accountId) {
		String query = "select from "+Account.class.getName()+" where accountId == '"+accountId+"'";
		List<Account> accounts =  (List<Account>) persistenceManager.newQuery(query).execute();
		if (accounts.size() > 0)
			return accounts.get(0).getRisk();
		return "-1";
	}
	
	@PUT 
	@Path("/updateRisk")
	public String updateRiskAccount(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		Extent<Account> e = persistenceManager.getExtent(accManager.Account.class, true);
		Iterator<Account> iter=e.iterator();
		while (iter.hasNext())
		{
			Account acc = (Account) iter.next();
			if(acc.getLastName().equals(queryParams.getFirst("nom")) && acc.getFirstName().equals(queryParams.getFirst("prenom")))
			{
				acc.setRisk(queryParams.getFirst("risk"));
				return "update succes";
			}
		}
		return "udpate fail";
	}
	
}
