package loanApproval;

public interface Url {
	public static final String BASE_URI = "http://1.loanapproval-63.appspot.com/";
	//public static final String BASE_URI = "http://localhost:8888/";
	public static final String CHECK_ACCOUNT_URI = "checkAccount/";
	public static final String APP_MANAGER_URI = "manageApprovals/";
	public static final String ACC_MANAGER_URI = "manageAccounts/";
}
