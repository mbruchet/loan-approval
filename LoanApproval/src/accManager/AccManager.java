package accManager;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import persistenceManager.PMF;

@Path("/")
public class AccManager {
	
	private PersistenceManager persistenceManager = PMF.getInstance().getPersistenceManager(); 

	@GET 
	@SuppressWarnings("unchecked")
	@Path("/list")
	@Produces(MediaType.TEXT_PLAIN)
	public String listAccounts() {		
		String query = "select from "+Account.class.getName();
		List<Account> accounts = (List<Account>) persistenceManager.newQuery(query).execute();
		
		StringBuilder page = new StringBuilder();
		if (accounts != null && accounts.size() != 0) {
			page.append("Liste des comptes ("+accounts.size()+")\n");
			for (Account account : accounts) {
				page.append(account.toString()).append("\n");
			}
		} else {
			page.append("Pas de comptes");
		}
		return page.toString();
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Path("/addAccount")
	public String addAccount(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		String accountId = (queryParams.getFirst("nom")+queryParams.getFirst("prenom")).toLowerCase();
		
		String query = "select from "+Account.class.getName()+" where accountId == '"+accountId+"'";
		List<Account> accounts = (List<Account>) persistenceManager.newQuery(query).execute();
		if (accounts.size() > 0) {
			return "Account exists already !";
		}
		float montant = 0;
		try {
			montant = Float.parseFloat(queryParams.getFirst("compte"));
		} catch (NumberFormatException e) {
			montant = 0;
		}
		Account account = new Account(accountId, queryParams.getFirst("nom"),
				queryParams.getFirst("prenom"), montant, queryParams.getFirst("risk"));
		persistenceManager.makePersistent(account);
		return "Account added !";
	}

	@SuppressWarnings("unchecked")
	@DELETE
	@Path("/deleteAccount/{accountId}")
	public String deleteAccount(@PathParam("accountId")String accountId) {
		String query = "select from "+Account.class.getName()+" where accountId=='"+accountId.toLowerCase()+"'";
		List<Account> accounts =  (List<Account>) persistenceManager.newQuery(query).execute();
		if (accounts.size()>0) {
			for (Account account : accounts)
				persistenceManager.deletePersistent(account);
			return "Success !";
		} 
		return "Account not found !";
	}
	
	@SuppressWarnings("unchecked")
	@PUT
	@Path("/creditAccount")
	public void creditAccount(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		String idPersonne = queryParams.getFirst("idPersonne");
		float montant = 0;
		try {
			montant = Float.parseFloat(queryParams.getFirst("montant"));
		}catch (NumberFormatException e) {}
		Transaction tx = persistenceManager.currentTransaction();
		tx.begin();
		String query = "select from "+Account.class.getName()+" where accountId=='"+idPersonne+"'";
		List<Account> accounts =  (List<Account>) persistenceManager.newQuery(query).execute();
		if (accounts.size()>0) {
			montant += accounts.get(0).getAccount();
			accounts.get(0).setAccount(montant);
		}
		tx.commit();
	}
}
