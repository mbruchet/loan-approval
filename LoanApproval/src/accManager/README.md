# AccManager

The **accManager** is a webservice CRUD on bank account, allow to CREATE, READ, UPDATE or DELETE accounts with a firstName, lastName, account id, amount of the account and the risk.

- /list => method to return all accounts
- /addAccount => method to create an account with JSON POST
- /deleteAccount/{accountId} => method to delete an account with his ID by using GET or DELETE HTTP Request