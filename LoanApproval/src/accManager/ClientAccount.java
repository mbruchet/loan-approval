package accManager;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.client.ClientConfig;

public class ClientAccount {
	
		private WebTarget webTarget;
	    private Client client;
	    private static final String BASE_URI = "http://1.loanapproval-63.appspot.com/manageAccounts/";

	    public ClientAccount() {
	        ClientConfig config = new ClientConfig();
    		client = ClientBuilder.newClient(config);
    		webTarget = client.target(BASE_URI);
	    }
	    
	    public String listAccounts() {
	    	return webTarget.path("/list").request().get(String.class);
	    }

	    public String add(String nom, String prenom, float compte, String risk) throws ClientErrorException {	    	
	    	MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
	    	formData.add("nom", nom);
	    	formData.add("prenom", prenom);
	    	formData.add("compte", String.valueOf(compte));
	    	formData.add("risk", risk);
	    	return webTarget.path("/addAccount").request().post(Entity.form(formData), String.class);
	    }
	    
	    public String removeAccount(String accountId) throws ClientErrorException {
	    	return webTarget.path("/deleteAccount/"+accountId).request(javax.ws.rs.core.MediaType.TEXT_PLAIN).delete(String.class);
	    }
	    
	    public void close() {
	        client.close();
	    }
	    
	    public static void main(String argv[]) throws InterruptedException{
	        ClientAccount client = new ClientAccount();
	        try{
	        	client.add("test", "test", 5000, Account.LOW_RISK);
	            client.removeAccount("protinjordan");
	            System.out.println(client.listAccounts());
	        }catch(Exception e){
	            System.out.println(e);
	        }
	        client.close();
	    }	
}
