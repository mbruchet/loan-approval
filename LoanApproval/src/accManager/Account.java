package accManager;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Account {
	
	public static final String HIGH_RISK = "high";
	public static final String LOW_RISK = "low";

	@PrimaryKey
	@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String lastName;
	
	@Persistent
	private String firstName;
	
	@Persistent
	private float account;
	
	@Persistent
	private String risk;
	
	@Persistent
	private String accountId;
	
	public Account(String accountId, String lastName, String firstName, float account, String risk) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.account = account;
		this.risk = risk;
		this.accountId = accountId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	@Override
	public String toString() {
		String s_risk = (risk == "low" ? "Low risk" : "High risk"); 
		StringBuilder chaine = new StringBuilder();
		chaine.append(firstName).append(" ")
					.append(lastName.toUpperCase())
					.append(" - ").append(s_risk)
					.append(" - ").append(account).append("�");
		return chaine.toString();
	}
}
