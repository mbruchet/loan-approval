# LoanApproval

This is an implementation of Loan Approval services realized from [Eclipse](https://www.eclipse.org/).

## Table of contents

1. [Project structure](#project-structure)
2. [Deploying](#deploying)
2. [How to use ?](#how-to-use-?)

## Project structure

- A client.php file and guzzle.phar in order to test.
- LoanApproval file (with GAE).
- loanApproval-heroku (with HEROKU)

## Deploying

### GAE

App's link on GAE : http://1.loanapproval-63.appspot.com

``` bash
> Service 1 (/manageAccounts/)
> Service 2 (/manageApprovals/)
> Service 3 (/checkAccount/)
```
> These services are on GAE !

### HEROKU

App's link on Heroku : https://fierce-spire-43574.herokuapp.com

``` bash
> Service 4 (LoanApproval)
```

> This service is on Heroku.

## How to use ?

### With clouds

By using a generated client : which is situed in the project root and called **client.php**.

You have to launch the following command in the project root :

``` bash
$ php5 client.php
```

## In local

By using a generated client : which is situed in the war folder from LoanApproval project and called **client.php**.

You have to launch the following command in the project root :

``` bash
$ php5 client.php
```